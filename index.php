<?php include_once "_partials/header.php"; ?>
<div class="page-header">
    <h1>VERY MUCH TODO LIST</h1>
</div>

<?php
$data = $database->select('items', ['id', 'text']);
?>

<ul id="item-list" class="list-group col-sm-6">
    <?php
    foreach ($data as $item) {
        echo '<li id="item-'. $item["id"] .'" class="list-group-item">';
        echo $item['text'];
        echo '<div class="controls pull-right">';
        echo '<a href="edit.php?id='.$item['id'].'" class="edit-link">edit </a>';
        echo '<a href="delete.php?id='.$item['id'].'" class="delete-link text-muted glyphicon glyphicon-remove"></a>';
        echo '</div>';
        echo '</li>';

    }
    ?>
</ul>

<form id="add-form" class="col-sm-6" method="post" action="_inc/add-item.php">
    <p class="form-group">
        <textarea name="message" id="text" rows="3" placeholder="Co mas v hlave?" class="form-control"></textarea>
    </p>
    <p class="form-group">
        <input type="submit" value="Ad new thing" class="btn-sm btn-danger">
    </p>
</form>
<?php include_once "_partials/footer.php"; ?>
