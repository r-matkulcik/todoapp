<?php
require_once "_inc/config.php";
$item = $database->get("items", "text", [
    "id" => $_GET['id']
]);

//if (!$item) {
//    show_404();
//}

include "_partials/header.php";

?>
<div class="page-header">
    <h1>VERY MUCH TODO DELETE</h1>
</div>

<div class="row">
    <form id="delete-form" class="col-sm-6" method="post" action="_inc/delete-item.php">
        <p class="form-group">
            <textarea name="message" id="text" rows="3" disabled class="form-control"
                      title="delete"><?php echo $item ?></textarea>
        </p>
        <p class="form-group">
            <input type="hidden" name="id" value="<?php echo $_GET['id'] ?>" class="">
            <input type="submit" value="delete item" class="btn-sm btn-danger">
            <span class="controls">
                <a href="<?php echo $base_url ?>" class="back-link text-muted">back</a>
            </span>
        </p>
    </form>
</div>

<?php include_once "_partials/footer.php"; ?>
