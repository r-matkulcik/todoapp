<?php
/**
 * Created by PhpStorm.
 * User: hlbinamyslenia
 * Date: 8.6.16
 * Time: 18:21
 */

// display errors
ini_set('display_startup_errors', 'On');
ini_set('display_errors', 'On');
//-1 display all
error_reporting(-1);
// autoload medoo by composer
require_once("vendor/autoload.php");

// Global variables
$base_url = ("http://localhost:8080");


// Magic PhpError Whoops shower :D
//\php_error\reportErrors();
//use Whoops\Handler\PrettyPageHandler;
//use Whoops\Handler\JsonResponseHandler;
//
//$run = new Whoops\Run;
//$handler = new PrettyPageHandler;


// Set the title of the error page:
//$handler->setPageTitle("Whoops! There was a problem.");
//
//$run->pushHandler($handler);
//
//// Add a special handler to deal with AJAX requests with an
//// equally-informative JSON response. Since this handler is
//// first in the stack, it will be executed before the error
//// page handler, and will have a chance to decide if anything
//// needs to be done.
//if (Whoops\Util\Misc::isAjaxRequest()) {
//    $run->pushHandler(new JsonResponseHandler);
//}
//
//// Register the handler with PHP, and you're set!
//$run->register();


// Initialize
$database = new medoo([
    'database_type' => 'mysql',
    'database_name' => 'todo',
    'server' => 'localhost',
    'username' => 'root',
    'password' => 'root',
    'charset' => 'utf8'
]);

// GLOBAL FUNCTIONS
function show_404()
{
    header("HTTP/1.0 404 Not Found");
    include_once "404.php";
    die();
}

function get_item()
{
    // if we have no id or id is empty
    if (!isset($_GET['id']) || empty($_GET['id'])) {
        return false;
    }

    // IT MUST BE GLOBAL INSIDE IN FUNCTION
    global $database;
    $item = $database->get("items", "text", [
        "id" => $_GET['id']
    ]);

    if (!$item) {
        return false;
    }

    return $item;

}

?>