<?php

require_once "_inc/config.php";
//if get id null run  404
$item = get_item();
if(! $item) show_404();
//
//if ( ! isset($_GET['id'])) {
//    show_404();
//}
//
//


include "_partials/header.php";

?>
<div class="page-header">
    <h1>VERY MUCH TODO edit</h1>
</div>

<div class="row">
    <form id="edit-form" class="col-sm-6" method="post" action="_inc/edit-item.php">
        <p class="form-group">
            <textarea name="message" id="text" rows="3" class="form-control" title="edit"><?php echo $item ?></textarea>
        </p>
        <p class="form-group">
            <input type="hidden" name="id" value="<?php echo $_GET['id'] ?>" class="">
            <input type="submit" value="Edit item" class="btn-sm btn-danger">
            <span class="controls">
                <a href="<?php echo $base_url ?>" class="back-link text-muted">back</a>
            </span>
        </p>
    </form>
</div>

<?php include_once "_partials/footer.php"; ?>
