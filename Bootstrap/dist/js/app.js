/**
 * Created by hlbinamyslenia on 6.6.16.
 */
(function ($) {


    var form = $('#add-form'),
        input = form.find('#text'),
        list = $('#item-list');
    input.val('').focus();

    /*
     * SETTINGS
     */
    var animation = {
        startColor: '#00bc8c',
        endColor: list.find('li').css('backgroundColor') || '#303030',
        delay: 200
    };
    form.on('submit', function (event) {
        event.preventDefault();

        var req = $.ajax({
            url: form.attr('action'),
            type: 'POST',
            data: form.serialize(),
            dataType: 'json'
        });

        /*if data succesfully create ajax and get homepage of site
         * Find Element LI from main page and auto add it to web with ajax
         * Added better URL global variable
         * */
        req.done(function (data) {

            if (data.status === 'Success') {
                $.ajax({url: baseURL}).done(function (html) {
                    var newItem = $(html).find('#item-' + data.id);

                    newItem.appendTo('.list-group')
                        .css({backgroundColor: animation.startColor})
                        .delay(animation.delay)
                        .animate({backgroundColor: animation.endColor});
                    input.val('').focus();
                });
            }
        });
    });


    input.on('keypress', function (event) {
        if (event.which === 13) {
            form.submit();
            return false;
        }
    });


    /*/
     *EDIT form
     */

    $('#edit-form').find('#text').select();
    /*/
     *DELETE form
     */
    $('#delete-form').on('submit', function (event) {
        return confirm("For sure?");
    });


}(jQuery));